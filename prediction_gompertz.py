#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 
"""
@author : Glorie M. WOWO
"""
import os

import numpy as np
from numpy.random import default_rng
rng = default_rng() # stuff for random sampling

import pandas as pd
import datetime
import json

# from growth_curve_classes import CurveLibrary, Predictor
from growth_curve_classes import CurveLibrary, Gompertz
from datetime import datetime, timedelta, date



#####################    Predictions  ####################################

def day_of_year (timestamp):
    return timestamp.timetuple().tm_yday  # returns 1 for January 1st



def gompertz_compare_model(out_path, data,data_ha,data_hist, year,interval=5, HIST=False, nbobs_=False, end_date_ = False):
#=================================================
    def create_folder(path):
        if not os.path.exists(path):
            os.makedirs(path)
    def get_dates(start_date, end_date, interval=interval):
        """
         Python function that takes two inputs, start_date and end_date, 
         and returns a list of dates between the start and end dates with 
         a 5 day interval
        """
        start_date = datetime.strptime(start_date, '%Y-%m-%d').date()
        end_date = datetime.strptime(end_date, '%Y-%m-%d').date()

        dates = []
        current_date = start_date
        while current_date <= end_date:
            dates.append(current_date)
            current_date += timedelta(days=interval)
        return dates
    
    def setup_historical_data ():
        ''' Recommended to run this before evaluations to ensure starting with clean
        datasets. '''

        data2019 = {}
        data2020 = {}
        data2021 = {}
        data2022 = {}
        g= pd.read_csv(data_hist, parse_dates = ['date'])
        lowercase = lambda x: str(x).lower()
        g.rename(lowercase, axis='columns', inplace=True)
#     print(data.columns)
        g.rename(columns={'id':'ID'}, inplace=True)
    
        g['year'] = g.date.dt.year
        g['month'] = g.date.dt.month
        g['day'] = g.date.dt.day
        g['DOY'] = g.date.apply(day_of_year)
        g.rename(columns = {'p':'proportion'},inplace=True)


        # Whether total_area is accurate is under discussion; similarly for what it should be called. 
        g['flooded'] = g.aoi_area * g.proportion


        g2019 = g[g.year == 2019].sort_values(['ID','DOY'])
        g2020 = g[g.year == 2020].sort_values(['ID','DOY'])
        g2021 = g[g.year == 2021].sort_values(['ID','DOY'])
        g2022 = g[g.year == 2022].sort_values(['ID','DOY'])
        

        for data, frame in zip([g2019,g2020,g2021, g2022],[data2019,data2020,data2021,data2022 ]):
            for id in g.ID.unique():
                curve = data[data.ID == id]
                curve.reset_index(inplace=True)
                frame[id] = curve[['ID','latitude','longitude','year','month','day','DOY','aoi_area','proportion','flooded']]

        return data2019,data2020,data2021,data2022 


    data2019, data2020, data2021, data2022 = setup_historical_data()

# dict that maps grid cell ID to a list of final level for each of three years
    estimated_finals = {k:[x.proportion.iat[-1] for x in [data2019[k],data2020[k],data2021[k], data2022[k]  ]]
                           for k in data2019.keys()}  

#++++++++++++++++++++++++++++++++++++++++++++++++++++
    pcol22_init_ha = pd.read_csv(data_ha)
    
    lowercase = lambda x: str(x).lower()
    data.rename(lowercase, axis='columns', inplace=True)
#     print(data.columns)
    data.rename(columns={'id':'ID'}, inplace=True)

    flooded = 'flooded.csv'
    datfres =[]
    #Last prediction at time t
    dfgompertz = pd.DataFrame()
    dfgompertz_col = pd.DataFrame()
    dfgompertz_all = pd.DataFrame()
    dfrest=pd.DataFrame()
    

    compt = 0
    
    
    p22 = data
    pcol22_init = p22.filter(regex=('\d{4}-?\d{2}-?\d{2}$'))
    
    nbobs = pcol22_init.shape[1] 

    dates_list = list(pcol22_init.columns)
    
    nbobs_ = len(get_dates(dates_list[0] , end_date_))
    
                
    if nbobs_==False:
        season_n_obs = nbobs
    else:
        season_n_obs = nbobs_
        
    
    while compt < len(dates_list):

        dfinit = pd.DataFrame()
        dat = dates_list[:compt+1]

        dfinit[dat] = pcol22_init [dat ]
        pcol22 = dfinit.copy()
        pcol22[['ID', 'geometry', 'latitude', 'longitude', 'aoi_area']] = p22[['ID', 'geometry', 'latitude', 'longitude', 'aoi_area']]

        pcol22 = pcol22.melt(id_vars = ['ID', 'geometry', 'latitude', 'longitude', 'aoi_area'], var_name = 'date', value_name = 'p')
        pcol22['year'] = year

        # Write it out
        pcol22.to_csv(out_path+'flooded.csv')

        # Read it in
        g=pd.read_csv(out_path+'flooded.csv',parse_dates = ['date'])
        g['year'] = g.date.dt.year
        g['month'] = g.date.dt.month
        g['day'] = g.date.dt.day
        g['DOY'] = g.date.apply(day_of_year)
        g.rename(columns = {'p':'proportion'},inplace=True)

        # Whether total_area is accurate is under discussion; similarly for what it should be called. 
        g['flooded'] = g.aoi_area * g.proportion

        # sort the data by gridcell ID and day of year
        g2022 = g.sort_values(['ID','DOY'])

        # make the ID column of type int
        g2022.ID =  g2022.ID  #g2022.ID.astype(int)

        # These are the columns we'll be working with
        columns = ['ID','latitude','longitude','year','month','day','DOY','aoi_area','proportion','flooded']

        # the number of observations we expect this season
        # We'll assume that the 2022 season will have 30 observations


        # the number of observations we've seen to date:
        obs_to_date = g[g['ID'] == g.iloc[0].ID].shape[0]


        data2022 = {}

        # make a dataframe for each gridcell.  It starts as an array of length n_obs
        # that's full of NaN. This allows us to write predictions for future observations
        # into the df, then fill in observations as we get them

        for ID in g.ID.unique():
            nanfull = pd.DataFrame(np.full((season_n_obs,len(columns)),np.nan), columns = columns)
            nanfull.ID = ID
            curve = g[g.ID == ID][columns]
            curve.reset_index(inplace=True,drop=True)
            nanfull[0:curve.shape[0]] = curve
            data2022[ID] = nanfull

        # Writing out the data 
        # g2022.to_csv(out_path+'g2022.csv')

        # create a curve library and a predictor
        cl30 = CurveLibrary(season_n_obs)
#         p2022 = Predictor(cl30, data2022, method = 'gompertz')
        p2022 =  Gompertz(cl30, data2022, method = 'gompertz')
       

        # Find how many observations we've had by looking at the first gridcell:
        print(f"We've seen {obs_to_date} five-day observations")

        #Now make predictions based on increasing numbers of observations:
        val_ =  int( dfinit.shape[1]/4) +1
        
        if HIST ==False:
            for k in range(val_,obs_to_date+1):
    #           p2022.predict_batch(K=k, estimated_finals = estimated_finals)
                p2022.predict_batch(K=k)
        else:
            for k in range(val_,obs_to_date+1):
                p2022.predict_batch(K=k, estimated_finals = estimated_finals)
#                 p2022.predict_batch(K=k)

        # Visualize to make sure nothing is amiss:
        cell34 = p2022.gridcell(34)

        # Produce a single dataframe that appends all the gridcell dataframes

        x = pd.concat(list(data2022.values()))

        # x.to_json(out_path+'predictions.json', orient='records')
        datf = pd.DataFrame.from_dict(x)
#         datf = pd.DataFrame.from_dict(x)
#         datf.to_csv ('/home/glorie/Documents/Gompertz_2023/data2023/gomp_output/predictions_csv.csv', index = None)
        
 #------------------- FILL IN DF


    #=======================================================================
        df = datf.copy()
        id_ = datf['ID'].unique()

        dflist = {}
    #     jour = [9,11,14,16,19,21,25,27, 30, 2]
    #     mois = [9, 9, 9, 9, 9, 9, 9, 9, 9, 10]
    #     doy= [252, 254, 257, 259, 262, 264, 268, 270, 273, 275]

        dfdate = pd.DataFrame()
        if end_date_==False:
            dfdate ['date'] = dates_list [compt+1:] #list(pcol22T.columns)
        else:
            start_date = dates_list[compt]
            end_date = end_date_
            dates = get_dates(start_date, end_date)
            dfdate = pd.DataFrame(dates[1:], columns=['date'])
        
        
        
        dfdate ['date'] = pd.to_datetime(dfdate ['date'])
        dfdate['year'] = dfdate['date'].dt.year
        dfdate['month'] = dfdate.date.dt.month
        dfdate['day'] = dfdate.date.dt.day
        dfdate['DOY'] = dfdate.date.apply(day_of_year)
             

        jour = dfdate['day'].to_list()
        mois = dfdate['month'] .to_list()
        doy =  dfdate['DOY'].to_list()
        
    
        # 
        
        
#         print(doy)

        total_area ={}

        # dftofil = pd.Dataframe()
        for i in id_:
            dflist [i] = df[df['ID']==i]
            dftmp =  dflist [i] [['ID','latitude','longitude','year','month','day','DOY','aoi_area']]
            col = ['latitude','longitude','year','month','day','DOY','aoi_area']

            lat =dflist [i] ['latitude'].values[0]
            lon = dflist [i] ['longitude'].values[0]
            year = dflist [i] ['year'].values[0]

            month = mois
            day = jour
            dy = doy

            aoiarea = dflist [i] ['aoi_area'].values[0]

            tofil = [ lat, lon, year,   month, day, dy , aoiarea ]


            dftmpnot_na = dftmp [dftmp ['year'].notnull()]

            dftmpisna = dftmp [dftmp ['year'].isna()]
            
            j=0
            for c in col:
# #                 print('c', c)
                 dftmpisna [c] = tofil[j]
                 j+=1

            dftmpnot_na = dftmpnot_na.append(dftmpisna)
            dflist [i] [ ['ID'] + col]  = dftmpnot_na


            total_area[i] = dflist[i].iloc[: , -1] .to_list() [-len(jour):] #[-1]

            df[df['ID']==i] = dflist [i] 

        dfout = pd.DataFrame()

        for k in id_:
                 dfout = dfout.append (  df[df ['ID'] == k ].iloc[:, -1].reset_index(). drop(columns='index').T  )

        dfout = dfout.reset_index().drop(columns='index')
        
        
        dates_list_ = [d.strftime('%Y-%m-%d') for d in get_dates(dates_list[0] , end_date_)]

        j=0
        for c in dfout.columns:
                dfout = dfout.rename ( columns = { c: dates_list_[j]  }) 
                j+=1

        print('dfres')
            #----------------
        dfres = pcol22.copy()
            
        dfres[  dates_list_ [:compt+1]  ] =   pcol22_init_ha [ dates_list_ [:compt+1] ]
        # pcol22v
#         dfres [dates_list] = dfout
        dfres [dates_list_ [compt+1:]  ] = dfout [dates_list_ [compt+1:] ] 
    

        dfres = dfres [dfres ['ID'].notnull()]
        
        res={}
        res['datf'] = datf
        res['dfres'] = dfres
            
        dfrest = dfres.dropna()
             
        dfres= dfres.dropna()
        create_folder(out_path+'/df')
        dfres.to_csv(out_path+'/df/df_'+dat[-1]+'.csv')
        


    #-----------------------------------------------------

    #         dfgompertz ['ID'] =  datf['ID']
        dfgompertz[ dat[-1] ] =   [dfres.iloc[: , -1].sum()] # datf.iloc[: , -1]

            #
        dfgompertz_coli=pd.DataFrame()
    
        dfgompertz_coli['time_t'] = [ dat[-1] ]
        dfgompertz_coli['area_t'] = [dfres.iloc[: , -1].sum()]
        dfgompertz_col = dfgompertz_col.append(dfgompertz_coli)


        compt+=1

    datfres.append(datf)
    
    
    dfgompertz.to_csv(out_path+'/prediction_growth.csv')
    dfgompertz_col.to_csv(out_path+'/prediction_growth_ts.csv')

    
    out ={}
    out['g'] = dfgompertz
    out['all'] = dfgompertz_col
    out['datf'] = datf
    out['dfres'] =dfrest
    return out