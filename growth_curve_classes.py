#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 31 14:46:56 2022

@author: prcohen
"""

import sys,copy
sys.path.append("/Users/prcohen/Pitt/Code/Mechanisms/mecha/classes/")

import numpy as np
from numpy.random import default_rng
rng = default_rng() # stuff for random sampling

import pandas as pd

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib as mpl
from matplotlib import gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable


######################  Curve Library #########################################

class CurveLibrary ():
    def __init__(self, n, bmin = 0, bmax = 10, b_levels = 20, cmin = .05, cmax = .95, c_levels = 10):
        self.n = n    
        self.b_levels = b_levels
        self.c_levels = c_levels
         
        self.b_vals = np.linspace(bmin,bmax,self.b_levels) 
        self.b_i = np.arange(self.b_levels).astype(int)
        self.c_vals = np.linspace(cmin,cmax,self.c_levels)
        self.c_i = np.arange(self.c_levels).astype(int)
        
        self.lib = np.zeros((self.b_levels,self.c_levels,self.n))
        
        ticks=np.arange(self.n)
        for b,bi in zip(self.b_vals,self.b_i):
            for c,ci in zip(self.c_vals,self.c_i):
                self.lib[bi,ci,:] = np.array([self.gompertz(1.0,b,c,t) for t in ticks])
                                              
    def gompertz (self,a,b,c,t):
        return a * np.e** -np.e**(b-c*t)
    
    def ser (self,b,c):
        return self.lib[b,c,:]


#################### Making and evaluating predictions ########################



class Predictor ():
    ''' This class generates one or more predicted sequences (e.g., Gompertz, 
    carry-forward, other methods) for each gridcell. `library` is a curve library, 
    `data` is a dict of pandas dataframes, one for each grid cell.    
    '''
    
    def __init__(self, library, data, method):
        self.lib = library
        self.curves = []
        self.indices = []
        # Make a 1D array of all curves and an aligned 1D array of curve parameter indices
        for b in self.lib.b_i:
            for c in self.lib.c_i:
                self.indices.append([b,c])
                self.curves.append(self.lib.lib[b,c,:])
        self.indices = np.array(self.indices)
        self.curves = np.array(self.curves)
                                   
        self.data = data
        self.gridcells = list(data.values())
        self.gc2id = {self.gridcell(i).ID.iloc[0]:i for i in range(len(self.gridcells))}
        self.sequences = np.array([x.proportion for x in self.gridcells])
        self.areas = np.array([x.aoi_area[0] for x in self.gridcells])
        self.method = method
        self.n = self.lib.n
        
    def best_k (self,seq,K,n_best):
        results = []
        for b in self.lib.b_i:
            for c in self.lib.c_i:
                t = self.lib.ser(b,c)
                s = seq[K-1]/max(t[K-1],.01)
                ts = t*s
                if all(ts < 1.05):
                    rmse = np.sum([(y - t)**2 for y,t in zip(seq[:K],ts[:K])])**.5
                    results.append([rmse,b,c,s])
        return sorted(results,key=lambda x: x[0])[:n_best]
    
    def predict (self, seq, K, n_best=15, lower=5, upper=13):
      ''' Given a sequence of K consecutive observations from one grid cell, this
      finds the n_best best-matching curves in library. It then sorts the curves
      by their final values and takes the trimmed mean of the "middle" curves, 
      meaning those curves whose sorted index is between lower and upper. It 
      returns this trimmed mean curve.  
      '''
      observed = seq[:K]
      best = self.best_k(seq,K,n_best)
      # rescale the best curves back to their original y axis 
      best_rescaled = np.array([self.lib.ser(x[1],x[2]) * x[3] for x in best])
      sorted_rescaled = sorted(best_rescaled,key = lambda x: x[-1])
      pruned_rescaled = sorted_rescaled[lower:upper]
      mean_rescaled = np.mean(pruned_rescaled,axis=0)
      return mean_rescaled
    
    def predict_batch (self,K):
        for g in self.gridcells:
            area = g.aoi_area[0]
            predicted_proportion = self.predict(g.proportion,K)
            g[self.method+'_'+str(K)] = predicted_proportion
            g[self.method+'_ha_'+str(K)] = predicted_proportion * area
        
    
    def plot_predicted (self, preds, seq, K, show_unseen = True, title=None):
        n = self.n
        seen = np.full(n, np.nan)
        unseen = np.full(n, np.nan)
        seen[:K] = seq[:K]
        
        if show_unseen and len(seq) > K:
            # some later elements of seq were not seen when making prediction
            r = np.arange(K,len(seq))
            unseen[r]=seq[r]
        
        fig, ax = plt.subplots(figsize=(4,3))
        x = np.arange(n)
        plt.plot(x,seen,label = 'seen', linewidth = 1)
        if show_unseen: plt.plot(x,unseen,label = 'unseen', linewidth = 1)
        for p in preds:  plt.plot(x,p[:n],label = 'gomp'+str(K), linewidth = 1, color = "green", linestyle='dashed')
        if title is not None : 
            plt.legend(title =title)
        else:
            plt.legend()
        plt.show()     
        
    def plot_batch (self, K, show_unseen = False):
        for g in self.gridcells:
            self.plot_predicted([g[self.method+'_'+str(K)]],g.proportion,K,show_unseen=show_unseen,title=g.ID[0])
    
    def accuracy (self,actual,predicted,K):
        ''' Let actual_n and pred_n be the lengths of the actual and predicted 
        sequence, respectively, assuming actual_n <= pred_n.  This returns the 
        squared and absolute difference between actual[-1] and pred[actual_n-1],
        that is, between the last actual value and its corresponding predicted
        value. It also returns the carry-forward difference.'''
        
        if type(actual) == pd.Series:   # could also be a np.array
            a = actual.iloc[-1]
        else:
            a = actual[-1]              # actual final value
        
        p_m = predicted[len(actual)-1]  # prediction from the model
        p_c = actual[K]                 # prediction from "carry forward"
        d_m = a - p_m                   # difference between actual and model prediction
        d_c = a - p_c                   # difference between actual and carry forward
        
        return[a,p_m,p_c,d_m,d_c]
        
    def accuracy_batch (self, K, actual, predicted, M = None, plot=False):
        ''' actual and predicted columns might be 'proportion' and, say, 'gompertz_9'
        or could be hectares, e.g., 'flooded' and 'gompertz_ha_9'.  K is the number
        of observations made before prediction (i.e. K = 9 will use 'gompertz_9'
        or 'gompertz_ha_9' to make the prediction).  M is which observation we
        want to predict.  In general, we want to predict the final observation, but
        we might want to predict an earlier one.  This is common in in-season 
        prediction when we expect to see, say, N = 30 observations, we've seen
        M = 18, and we want to retrospectively assess the accuracy of predicting 
        the Mth observation after seeing K observations.'''
        
        if M is None:
            # predict the final observation
            acc = [self.accuracy(g[actual],g[predicted],K) for g in self.gridcells]
        else:
            # predict the final of M observations 
            acc = [self.accuracy(g[actual][:M],g[predicted][:M],K) for g in self.gridcells]
        
        RMSE = np.mean([x[3]**2 for x in acc])**.5  # model-based root mean squared error
        ME =   np.mean([x[3] for x in acc])         # mean model-based error
        MAE  = np.mean([abs(x[3]) for x in acc])    # mean absolute model-based error
        MCF  = np.mean([x[4] for x in acc])         # mean carry-forward error
        MACF = np.mean([abs(x[4]) for x in acc])    # mean absolute carry-forward error
        
        
        if plot:
            actual = [x[0] for x in acc]
            self.scatterplot([x[1] for x in acc],actual,'predicted','actual','model-based prediction')
            self.scatterplot([x[2] for x in acc],actual,'predicted','actual','carry-forward prediction')
            self.scatterplot([x[1] for x in acc],[x[2] for x in acc],'model-based predicted','carry-forward predicted','comparing model-based and carry-forward')
            
        return RMSE,ME,MAE,MCF,MACF
    
    def scatterplot (self,x,y,xlabel,ylabel,title):
        plt.scatter(x,y, s=5, alpha=1)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.title(title)
        plt.show()
    
    def gridcell (self,i):
        return self.gridcells[i]
    
    def gridcell_from_id (self, id):
        return [self.gridcells[i] for i in range(len(self.gridcells)) if self.gridcells[i].iloc[0].ID == id]
    
class Gompertz(Predictor):
    
    def curve_score (self,seq,curve,est_finals,K):   
        ''' Returns a weighted average of 1) the RMSE between the sequence of 
        observations seen so far and the curve, and 2) the distance between between
        an estimate of the final value of the sequence and the final value of the
        curve.  The weight initially favors 2 but as the sequence gets longer it
        favors 1.  This helps to ensure that predictions made from very little data
        are biased toward the estimate of the final value.'''
        m = max(.0001, (np.mean((seq[:K]-curve[:K])**2)**0.5))
        if est_finals is not None:
            w = K/self.n
            p = max(.0001, np.min([np.abs(ef - curve[-1]) for ef in est_finals]))
            return -(w * -np.log(m) + (1-w) * -np.log(p))
        else:
            return m
    
    def predict_batch (self, K, n_best = 5, estimated_finals = None):
        ''' Fully vectorized scaling of the entire curve library, much faster than 
        earlier versions. estimated_finals, if specified, is used to bias the 
        curve selection when thre are very few observations. It should be a dict 
        mapping grid ID to a list of final levels of that grid in previous years 
        (e.g., {5509970: [.4,.28, .01],...})'''
        
        best_curves_for_seq = []
        
        
        # Align each sequence to each curve and calculate RMSE
        for gc in self.gridcells:
            
            ID = gc.ID.iat[0]
            est_final = estimated_finals[ID] if estimated_finals is not None else None
            seq = np.array(gc.proportion)
            
            
            # k_points is a vector of the Kth point in each library curve
            # This occasionally throws a divide-by-zero warning, but resist the
            # temptation to replace zeros by a small constant!  Doing so has bad
            # downstream effects
            k_points = self.curves[:,K-1]   
            
            # rescale all the library curves in one go to align the k_points with
            # the Kth point in seq
            scaled_curves = self.curves * (seq[K-1] / k_points)[:,np.newaxis]
            
            # remove scaled curves whose values exceed 1.0
            pruned_scaled = scaled_curves[scaled_curves[:,-1] <= 1]
            
            # score RMSE between curve and seq for all points 0...K-1
            # save the RMSE score, the indices and the scaled curve for later
            rmse = [[self.curve_score(seq,sc,est_final,K),sc]
                for sc in pruned_scaled]
            
            # sort the result by rmse score (lower is better) and save to best_curves
            rmse = sorted(rmse,key=lambda x: x[0])[:n_best]
            
            mean_best = np.mean([x[1] for x in rmse],axis=0)
            
            area = gc.aoi_area[0]
            gc[self.method+'_'+str(K)] = mean_best
            gc[self.method+'_ha_'+str(K)] = mean_best * area