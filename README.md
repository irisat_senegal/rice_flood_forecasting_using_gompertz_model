`Title - Rice Flood Forecasting using Gompertz Model: Predicting Dates and Areas`

**Description**

This project aims to predict the dates and areas of future flooding using a mathematical model known as the Gompertz Model. The Gompertz function is a type of mathematical model that is commonly used in biology and other fields to describe growth processes that slow down over time. The model will be trained on historical flooding data from rice crops, and the output will be data of the predicted dates and areas of future flooding events.
This will will provide valuable insight for rice farmers and agriculture planners to make informed decisions and optimize crop production.


**Requirements**

- python 3.x
- numpy
- pandas


**Inputs :** 

- out_path: the path where the output files will be saved
- data: the input data
- data_ha: the input data for hectare calculations
- data_hist: the historical data
- year: the year being considered for the predictions
- interval: the interval in days between the dates being considered (default is 5)
- HIST: a flag indicating whether the historical data should be used (default is False)
- nbobs_: a flag indicating whether the number of observations should be used (default is False)
- end_date_: the end date for the predictions (default is False)


**Outputs:**

The function generates dataframes that contain the predicted values for a specified time period, based on the historical data of images.


**The function contains several helper functions:**

- create_folder: a function to create a folder at the specified path if it does not exist
- get_dates: a function that takes two dates and returns a list of dates with a specified interval between them
- setup_historical_data: a function that sets up the historical data by reading it from a file, transforming the data, and returning a dictionary of the data for each year


**The main logic of the function then follows:**

- The historical data is set up using the setup_historical_data function
- A dictionary of estimated final levels is created for each grid cell ID based on the historical data
- The input data is processed and renamed
- A list of dates between the start and end dates is generated
- The predictions are made using the Gompertz growth model
- The predictions are saved to a file


**Author**

Glorie Metsa WOWO

*Scientific Officer - AI and remote sensing at ICRISAT*

**Contributors**

The Team Behind the HEURISTIC Project